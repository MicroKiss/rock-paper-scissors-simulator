#pragma once

#include <iostream>
#include <set>
#include <queue>
#include <random>
#include <algorithm>

enum shape
{
    rock = 0,
    paper = 1,
    scrissor = 2
};

class player
{
public:
    static unsigned allshapes[3];

    std::string name;
    unsigned wins = 0, total = 0; //how many wins in the all matches and all the matches
    std::multiset<shape> plays;
    std::multiset<shape> receives;

    size_t cachesize = 50;
    std::deque<shape> lastreceived;
    std::deque<shape> lastplayed;

    virtual shape Strategy() = 0;
    virtual ~player(){};

    double Winrate() const
    {
        return double(wins) / double(total);
    }

    shape Play();
    void Receive(const shape &gotshape);
    std::string Name() const { return name; };
};

class playerrandom : public player
{
    std::mt19937 *gen;
    std::uniform_int_distribution<> dis{0, 2};

public:
    playerrandom(std::mt19937 *gen) : gen(gen) { name = "random"; }
    virtual shape Strategy() override
    {
        return shape(dis(*gen));
    }
};

class playerstubborn : public player
{

    shape ONE;

public:
    playerstubborn(shape TheOne) : ONE(TheOne) { name = "stubborn"; }
    virtual shape Strategy() override
    {
        return ONE;
    }
};

class playerlastreceived : public player
{
    shape ONE;

public:
    playerlastreceived() { name = "lastreceived"; }
    virtual shape Strategy() override
    {
        return (lastreceived.size() ? lastreceived.back() : shape::rock);
    }
};

class playerlasttwo : public player
{
    std::mt19937 *gen;
    std::uniform_int_distribution<> dis{0, 2};

public:
    playerlasttwo(std::mt19937 *gen) : gen(gen) { name = "lasttwocounter"; }
    virtual shape Strategy() override
    {
        if (lastreceived.size() > 2)
        {
            shape last = lastreceived.back();
            lastreceived.pop_back();
            shape last_1 = lastreceived.back();
            lastreceived.push_back(last);
            if (last_1 == last)
                return shape((last + 1) % 3);
        }

        return shape(dis(*gen));
    }
};

class playersequence : public player
{
    shape iteration[3] = {shape::rock,
                          shape::paper,
                          shape::scrissor};
    short pos = 0;

public:
    playersequence()
    {
        name = "sequence";
        std::random_shuffle(iteration, iteration + 2);
    }
    virtual shape Strategy() override
    {
        pos = ++pos % 3;
        return iteration[pos];
    }
};
