
#include <time.h>
#include "player.h"
#include <vector>
#include <algorithm>

void Match(player *A, player *B);
void StatsShow();

unsigned player::allshapes[3] = {};

int main()
{
    std::cout << "begin\n";
    std::mt19937 gen(time(0));

    std::vector<player *> players;

    players.push_back(new playerrandom(&gen));
    //players.push_back(new playerstubborn(shape::rock));
    players.push_back(new playerlastreceived());
    players.push_back(new playerlasttwo(&gen));
    players.push_back(new playersequence());

    //everybody plays with everyone  i times
    for (auto it1 = players.begin(); it1 != players.end(); ++it1)
        for (auto it2 = players.begin(); it2 != players.end(); ++it2)
            if (it1 != it2) //should they play themselfs
                for (int i = 0; i < 10000; ++i)
                    Match(*it1, *it2);

    std::cout << "Winrates: ";
    std::for_each(players.begin(), players.end(), [](player *P) { std::cout << P->Name() << " " << P->Winrate() << " "; });
    std::endl(std::cout);

    StatsShow();

    return 1;
}