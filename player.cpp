#include "player.h"

shape player::Play()
{
    if (lastplayed.size() == cachesize)
        lastplayed.pop_front();

    shape newshape = Strategy();
    lastplayed.push_back(newshape);

    plays.insert(newshape);

    player::allshapes[int(newshape)]++;

    return newshape;
}

void player::Receive(const shape &gotshape)
{
    ++total;
    if (lastreceived.size() == cachesize)
        lastreceived.pop_front();

    lastreceived.push_back(gotshape);
    receives.insert(gotshape);

    if (((int(gotshape) + 1) % 3) == int(lastplayed.back())) //rock = 0 , paper = 1
        ++wins;
}

///*************************
void Match(player *A, player *B)
{
    shape a = A->Play();
    shape b = B->Play();

    A->Receive(b);
    B->Receive(a);
}

void StatsShow()
{
    std::cout << "rock: " << player::allshapes[0] << "\n";
    std::cout << "paper: " << player::allshapes[1] << "\n";
    std::cout << "scrissor: " << player::allshapes[2] << "\n";
}

void StatsClear()
{
    player::allshapes[0] = player::allshapes[1] = player::allshapes[2] = 0;
}
