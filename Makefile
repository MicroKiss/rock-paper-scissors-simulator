# Programs & tools (Windows)
CXX     = g++
RM      = rmdir /s /q
MKDIR   = mkdir
CP      = copy /y

# Flags & options
CFLAGS  = -O2 -std=c++17 -pedantic

# Output
OBJDIR  = build
MAIN    = simulator.exe
TARGET  = $(OBJDIR)/$(MAIN)

_OBJ = main.o player.o
OBJ = $(patsubst %, $(OBJDIR)/%, $(_OBJ))

# Make targets
.PHONY: all
all: $(TARGET)

$(OBJDIR):
	$(MKDIR) $(OBJDIR)

$(OBJDIR)/player.o: player.cpp player.h
	$(CXX) -c -o $@ $< $(CFLAGS) $(INCLUDE)

$(OBJDIR)/main.o: main.cpp player.h
	$(CXX) -c -o $@ $< $(CFLAGS) $(INCLUDE)

$(TARGET): $(OBJDIR) $(OBJ)
	$(CXX) -o $@ $(OBJ) $(CFLAGS)

.PHONY: clean
clean:
	$(RM) $(OBJDIR)
